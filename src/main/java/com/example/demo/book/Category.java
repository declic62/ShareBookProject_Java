package com.example.demo.book;

public class Category {
    private String label;
    private long ref;

    // Constructor
    public Category(String label, long ref) {
        this.label = label;
        this.ref = ref;
    }

    public long getRef() {
        return ref;
    }

    public void setRef(long ref) {
        this.ref = ref;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {

        this.label = label;
    }

}
