package com.example.demo.book;

public class Book {

    private String title;
    private Category category;

    public Category getCategory() {

        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }


    public String getTitle() {

        return title;
    }

    public void setTitle(String title) {

        this.title = title;
    }


}
