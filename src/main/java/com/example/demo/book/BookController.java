package com.example.demo.book;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class BookController {
    Category heroicFantasy;

    @GetMapping(value = "/books")
    public ResponseEntity<Book[]> addBook(){
        Book book = new Book();
        Book[] listOfBooks = {book};
        book.setTitle("game Of Thrones");
        book.setCategory(new Category("heroic Fantasy", 1001));

        return new ResponseEntity<Book[]>(listOfBooks, HttpStatus.OK);
    }

    @PostMapping(value = "/books")
    public ResponseEntity<Book> addBook(Book book){
        // TODO Persist
        return new ResponseEntity<Book>(book, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/books/{bookId}")
    public ResponseEntity<Book> deleteBook(@PathVariable("bookId") String bookId){
        // TODO delete depuis bookId
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PutMapping(value = "/books/{bookId}")
    public ResponseEntity<List<Book>> updateBook(@PathVariable("bookId") String bookId, Book book){
        // TODO delete depuis bookId
        return new ResponseEntity<>(List.of(book), HttpStatus.OK);
    }
}
