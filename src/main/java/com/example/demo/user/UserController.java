package com.example.demo.user;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {

    @PostMapping(value = "/user")
    public ResponseEntity addUser(User user){
        User userTest = new User("bruno@gmail.com");
        return new ResponseEntity<>(userTest, HttpStatus.CREATED);
    }
}
